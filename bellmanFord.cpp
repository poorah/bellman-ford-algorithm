#include <bits/stdc++.h>

using namespace std ;

class Graph {
    //properties:
    public:
        map<char, list<pair<char, int>>> adj; // weighted graph
        map<char, int> distance;
        map<char, int> visited;
        int nodes;
    //methods:
        void addEdge(char v, char u, int weight); // adding weight
        void bellmanFord(char s);
};

void Graph::addEdge(char v, char u, int weight) {
    //todo
    adj[v].push_back(make_pair(u, weight));
}

void Graph::bellmanFord(char s) {
    //todo
    map<char, list<pair<char, int>>>::iterator i;
    list<pair<char, int>>::iterator j;
    for(i = adj.begin(); i != adj.end(); i++) {
        for(j = adj[i->first].begin(); j != adj[i->first].end(); j++) {
            distance[j->first] = INT_MAX;
            visited[j->first] = 0;
        }
    }

    distance[s] = 0;
    
    for(int c = 1; c <= nodes; c++) {
        char u = s;
        
        queue<char> pq;
        pq.push(s);
        visited[s]++;
        
        while(!pq.empty() && visited[pq.front()] <= nodes) {
            
            for(j = adj[u].begin(); j != adj[u].end(); j++) {
                
                pq.push(j->first);
                
                if(distance[j->first] > (distance[u] + j->second)) {
                    
                     distance[j->first] = (distance[u] + j->second);
                }
            }
            
            u = pq.front();
            visited[u]++;
            pq.pop();
        }
    }

    map<char, int>::iterator k;
    for(k = distance.begin(); k != distance.end(); k++) {
        cout << k->first << "    :\t   " << k->second << endl;
    }
    
}

int main() {

    Graph g ;
    
    g.nodes = 5;
    g.addEdge('A', 'B', -1);
    g.addEdge('B', 'E', 2);
    g.addEdge('E', 'D', -3);
    g.addEdge('D', 'B', 1);
    g.addEdge('B', 'D', 2);
    g.addEdge('D', 'C', 5);
    g.addEdge('A', 'C', 4);
    g.addEdge('B', 'C', 3);
    
    cout << "Node : \t distance" << endl;
    g.bellmanFord('A');
    
    return 0;
}
